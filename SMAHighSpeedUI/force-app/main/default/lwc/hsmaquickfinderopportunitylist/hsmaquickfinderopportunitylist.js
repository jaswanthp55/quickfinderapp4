import { LightningElement, track, wire ,api} from 'lwc';
import { refreshApex } from '@salesforce/apex';
import serachCons from '@salesforce/apex/hsmapeoplesearchmaincls_venkat.retriveContacts';
import getOpps from '@salesforce/apex/hsmapeoplesearchmaincls_venkat.getOpportunitiesList';
import getTasksList from '@salesforce/apex/hsmapeoplesearchmaincls_venkat.getTasksList';
import getTasksHistoryList from '@salesforce/apex/hsmapeoplesearchmaincls_venkat.getTasksHistoryList';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
export default class Hsmaquickfinderopportunitylist extends LightningElement {
    @track data;
    @api selectedconaccId;
    @api selectedconId='';
    @api selectedoppId;
    result;
    //this is initialize for 1st page
    @api page = 1;
    //it contains all the Product records.
    @track items = [];
    //To display the data into datatable
    @track data = [];
    //holds column info.
    @track columns;
    //start record position per page
    @track startingRecord = 1;
    //end record position per page
    @track endingRecord = 0;
    //10 records display per page
    @api pageSize = 5;
    @track setPagination;
    //total count of record received from all retrieved records
    @api totalRecountCount = 0;
    //total number of page is needed to display all records
    @track totalPage = 0;
    @track maxrowselection = 1;
   //To display the column into the data table
   @track opportunities;
   @track tasks;
   @track error;
   @track error1;
   @track error2;
   @track searchKey;
   @track columns = [
    {
     label: 'Name',
     fieldName: 'Name',
     type: 'text',
     sortable: "true"
     },
     {
        label: 'STAGE',
        fieldName: 'StageName',
        type: 'text',
        sortable: "true"
    },
    {
        label: 'TYPE',
        fieldName: 'Type',
        type: 'text',
        sortable: "true"
    },
    {
        label: 'CLOSE DATE',
        fieldName: 'CloseDate',
        type: 'date',
        sortable: "true"
    },   
    ];   
    //call the apex method and pass the search string into apex method.
    connectedCallback() {      
        console.log('Child Component selectedconId*******'); 
        //this.selectedconId='0038A00000R6sguQAB';
        //console.log('Child Component selectedconId*******'+this.selectedconId); 
    }
    wiredOpps;
    @wire(getOpps,{conid:'$selectedconId'})
    fetchOpps(result) {
        console.log('insideftech opps********');
        this.wiredOpps = result;   
        if (result.data) {        
            this.items = result.data; 
            this.setPagination = false; 
            this.totalRecountCount = result.data.length;
            this.totalPage = Math.ceil(this.totalRecountCount / this.pageSize);
            this.data = this.items.slice(0,this.pageSize);
            this.endingRecord = this.pageSize;
            this.error = undefined;  
            console.log('opps++'+this.items);
          if(this.totalRecountCount > this.pageSize){
                console.log('entered to set pagination');
                this.setPagination = true;
            }
            this.tasks = undefined;
            this.tasksHistory = undefined; 
        } else if (result.error) {
            console.log('inside else error********');
            this.error = result.error;
            this.data = undefined; 
            this.totalRecountCount=0; 
            this.tasks = undefined;
            this.selectedoppId=undefined;
            this.tasksHistory = undefined;         
        }
        else{
            console.log('inside else********');
            this.totalRecountCount=0;
            this.tasks = undefined;
            this.selectedoppId=undefined;
            this.tasksHistory = undefined; 
            this.data = undefined; 
        }    
    }
    //this method is called when you clicked on the previous button
    previousHandler() {
        if (this.page > 1) {
            this.page = this.page - 1; //decrease page by 1
            this.displayRecordPerPage(this.page);
        }
    }
    //this method is called when you clicked on the next button
    nextHandler() {
        if((this.page<this.totalPage) && this.page !== this.totalPage){
            this.page = this.page + 1; //increase page by 1
            this.displayRecordPerPage(this.page);
        }
    }
    handleFirst() {  
        this.page = 1;  
        this.displayRecordPerPage(this.page);
    }  
    handleLast() {  
        this.page = this.totalPage;  
        this.displayRecordPerPage(this.page);
    }  
    handleSortdata(event) {
        // field name
        this.sortBy = event.detail.fieldName;

        // sort direction
        this.sortDirection = event.detail.sortDirection;

        // calling sortdata function to sort the data based on direction and selected field
        this.sortData(event.detail.fieldName, event.detail.sortDirection);
    }

    sortData(fieldname, direction) {
        // serialize the data before calling sort function
        let parseData = JSON.parse(JSON.stringify(this.data));

        // Return the value stored in the field
        let keyValue = (a) => {
            return a[fieldname];
        };

        // cheking reverse direction 
        let isReverse = direction === 'asc' ? 1: -1;

        // sorting data 
        parseData.sort((x, y) => {
            x = keyValue(x) ? keyValue(x) : ''; // handling null values
            y = keyValue(y) ? keyValue(y) : '';

            // sorting values based on direction
            return isReverse * ((x > y) - (y > x));
        });

        // set the sorted data to data table data
        this.data = parseData;

    }
    handleConfig(){
        var el = this.template.querySelector('lightning-datatable');
        var selected = el.getSelectedRows();
        }
    //this method displays records page by page
    displayRecordPerPage(page){
        this.startingRecord = ((page -1) * this.pageSize) ;
        this.endingRecord = (this.pageSize * page);
        this.endingRecord =
        (this.endingRecord > this.totalRecountCount) ?   this.totalRecountCount : this.endingRecord;
        this.data = this.items.slice(this.startingRecord,   this.endingRecord);
        this.startingRecord = this.startingRecord + 1;
    }
    handleContactName(event) {
       this.strSearchContactName = event.target.value;
       return refreshApex(this.result);
    }
    /* eslint-disable no-console */
    // eslint-disable-next-line no-console
    //this method holds the selected product.
    handleConfig(){
    var el = this.template.querySelector('lightning-datatable');
    var selected = el.getSelectedRows();
    }
    getSelectedRecords(event) {
        event.preventDefault();
        // getting selected rows
        const selectedRows = event.detail.selectedRows;
        window.console.log('selectedRows ====> ' +selectedRows);
        this.recordsCount = event.detail.selectedRows.length;
        // this set elements the duplicates if any
        window.console.log('recordsCount ====> ' +this.recordsCount);
        let oppIds = new Set();
        var oppIds1='';
        // getting selected record id
        for (let i = 0; i < selectedRows.length; i++) {
            oppIds.add(selectedRows[i].Id);
            oppIds1=selectedRows[i].Id;
            window.console.log('inside for loop ====> ' +oppIds);
            window.console.log('oppIds1====> ' +oppIds1);
        }
        window.console.log('oppIds ====> ' +oppIds);
        window.console.log('oppIds12====> ' +oppIds1);  
        window.console.log('selectedRecords ====> ' +this.selectedRecords);
        this.tasks = undefined;
        this.tasksHistory = undefined;
        var str = oppIds1;
        console.log('str*******'+str);
        //var res = str.substring(0,15);
        var res = str;
        console.log('res*******'+res);
        var recId = res;
        console.log('recId*******'+recId);
        this.detailid=recId;
        console.log('detailid*******'+this.detailid);
        this.selectedoppId=recId;
        console.log('selectedoppId*******'+this.selectedoppId);

        getTasksList({ oppid: recId })
        .then(TaskList => {
        this.isLoaded = false;
        this.tasks = TaskList;
        this.error2 = undefined;
        })
        .catch(error => {
        this.error2 = error;
        this.tasks = undefined;
        this.isLoaded = false;
            this.tasksHistory = undefined;
            window.console.log('error =====> '+JSON.stringify(error));
            if(error) {
                console.log(error);
                this.errorMsg = error.body.message;
            }
            const event = new ShowToastEvent({
                variant: 'error',
                //message: 'No Activities present for the selected opportunity.',
            });
            this.dispatchEvent(event);
        });
        getTasksHistoryList({oppid: recId})
        .then(result => {
            this.tasksHistory = result;
            console.log(this.tasksHistory);
        })
        .catch(error => {
            this.tasksHistory = undefined;
            window.console.log('error =====> '+JSON.stringify(error));
            if(error) {
                console.log(error);
                this.errorMsg = error.body.message;
            }
            const event = new ShowToastEvent({
                variant: 'error',
                //message: 'No Task History present for the selected opportunity.',
            });
            this.dispatchEvent(event);
        });
        //return refreshApex(this.Opportunities);
    }
    /* private property is used to show/hide opp popup */
    @track bShowModal = false;
    /* JS function to open opp model  */ 
    openModal() {    
        // to open modal window set 'bShowModal' tarck value as true
        this.taskObject = {};
        console.log('open Modal check taskObject+++++'+JSON.stringify(this.taskObject));
        this.bShowModal = true;
    }
    /* JS function to close opp model */
    closeModal() {    
        // to close modal window set 'bShowModal' tarck value as false
        this.bShowModal = false;
    }
    @track newOppId;
    handleSuccess(event) {       
        console.log('inside handle sucess++');
        /*console.log('check event'+JSON.stringify(event));*/
        const evt = new ShowToastEvent({
            title: 'Success!',
            message: "Opportunity saved successfully.",
            variant: 'success'
        });
        this.dispatchEvent(evt); 
        //this.refreshData();  
        this.bShowModal = false;   
        return refreshApex(this.wiredOpps);     
    }
    refreshData() {
        console.log('inside refresh data*********');
        return refreshApex(this.wiredOpps);
    }
    navigateToRecordViewPage(event) {
        var taskId = event.target.dataset.recordId;    
        console.log('inside navigaterecordviewpage******'+taskId);
        window.open('https://horizonbcbs--smadevteam.lightning.force.com/' + taskId );
    }
}