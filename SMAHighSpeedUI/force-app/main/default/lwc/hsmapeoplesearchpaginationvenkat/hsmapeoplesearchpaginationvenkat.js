import { LightningElement, api } from 'lwc';

export default class Hsmapeoplesearchpaginationvenkat extends LightningElement {

  // Api considered as a reactive public property.  
  @api totalrecords;  
  @api currentpage;  
  @api pagesize;  

  // Following are the private properties to a class.  
  lastpage = false;  
  firstpage = false;  
  
  // getter  
  get showFirstButton() {  
    console.log('>>>>>>>>> entered showfirstbutton method');
    console.log('>>>>>>>>> showfirstbutton currentpage number: ' + this.currentpage);
    if (this.currentpage === 1) {  
      console.log('>>>>>>>>> showfirstbutton true');
      return true;  
    }  
    return false;  
  }  
  // getter  
  get showLastButton() {  
    console.log('>>>>>>>>> entered showlasttbutton method');
    console.log('>>>>>>>>> showlasttbutton totalrecords: ' + this.totalrecords);
    console.log('>>>>>>>>> showlasttbutton pagesize: ' + this.pagesize);
    console.log('>>>>>>>>> showlasttbutton currentpage number: ' + this.currentpage);
    if (Math.ceil(this.totalrecords / this.pagesize) === this.currentpage) {
      console.log('>>>>>>>>> showlastbutton true');
      return true;  
    }  
    return false;  
  }  


    //this method handles the previous button with the help of dispatchEvent
    previousHandler() 
    {
     this.dispatchEvent(new CustomEvent('previous'));
    }
//this method handles the Next button with the help of dispatchEvent
    nextHandler() 
    {
     this.dispatchEvent(new CustomEvent('next'));
    }
    
    handleFirst() {  
        this.dispatchEvent(new CustomEvent('first'));  
      }  
      handleLast() {  
        this.dispatchEvent(new CustomEvent('last'));  
      }  
}