({
	getSAML : function(component) {
		var recordId = component.get("v.recordId");
        var action = component.get("c.getSAMLString");
        action.setParams({"oppId" : recordId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() &&  state === "SUCCESS") {
                component.set("v.SAMLResponse", response.getReturnValue().SAMLResponse);
                component.set("v.isDisabled", response.getReturnValue().IsDisabled);
                component.set("v.ErrorMessages", response.getReturnValue().ErrorMessages);
                console.log(JSON.stringify(response.getReturnValue().SAMLResponse));
                console.log(JSON.stringify(response.getReturnValue().IsDisabled));
                console.log(JSON.stringify(response.getReturnValue().ErrorMessage));
            }
            else {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }

            }  
        });

        $A.enqueueAction(action);
	}
})